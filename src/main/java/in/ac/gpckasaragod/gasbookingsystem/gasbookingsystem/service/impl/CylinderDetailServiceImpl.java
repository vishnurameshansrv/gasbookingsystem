/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.gasbookingsystem.gasbookingsystem.service.impl;

import in.ac.gpckasaragod.gasbookingsystem.ui.CylinderDetails;
import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */


public class CylinderDetailServiceImpl extends ConnectionServiceImpl implements CylinderDetailsService {
    public String saveCylinderDetail(Integer id, String companyname, String quantity,String cylinderamount) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO CYLINDER_DETAILS (ID,COMPANY_NAME,QUANTITY,CYLINDER_AMOUNT) VALUES " 
                    + "('"+id+ "',"+companyname+ ",'"+quantity+"','"+cylinderamount+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(CylinderServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "Save failed";
        }
}
    public CylinderDetails readCylinderDetail(Integer Id){
        CylinderDetails cylinderDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM CYLINDER_DETAILS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                String companyname = resultSet.getString("COMPANY_NAME");
                String quantity = resultSet.getString("quantity");
                String cylinderamount = resultSet.getString("cylinderamount");
                cylinderDetails = new CylinderDetails(id,companyname,quantity,cylinderamount);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CylinderServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cylinderDetails;
       
    }
     public List<CylinderDetails> getAllCylinderDetail(){
        List<CylinderDetails> cylinderdetails = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT CYLINDER_DETAILS.ID,COMPANY_NAME,QUANTITY,CYLINDER_AMOUNT FROM CYLINDER_DETAILS JOIN CUSTOMERDEATAILS ON CUSTOMERDETAILS.ID=CYLINDER_DETAILS.ID";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             Integer id = resultSet.getInt("ID");
                String companyname= resultSet.getString("COMPANY_NAME");
                Integer quantity = resultSet.getInt("QUANTITY");
                String cylinderamount = resultSet.getString("CYLINDER_AMOUNT");
                CylinderDetails cylinderDetails = new CylinderDetails(id,companyname,quantity,cylinderamount);
                cylinderdetails.add(cylinderDetails);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(CylinderServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return cylinderdetails;
    
    }
     
     public String updateCylinderDetail(Integer id,String companyname,Integer quantity,String cylinderamount){
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE CYLINDER_DETAILS SET ID='"+id+"',COMPANY_NAME="+companyName+",QUANTITY='"+quantity+"',CYLINDER_AMOUNT='"+cylinderAmount+"';
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else
            return "Updated successfully";
        }catch(SQLException ex){
            return "Update failed";
        }
    }
     public String deleteCylinderDetail(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM CYLINDER_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
           return "Delete failed";
        }
     
}

    private Connection getConnection() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}


