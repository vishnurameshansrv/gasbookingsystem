/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.gasbookingsystem.gasbookingsystem.service.impl;

import in.ac.gpckasaragod.gasbookingsystem.ui.CustomerDetails;
import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */


public class CustomerDetailServiceImpl extends ConnectionServiceImpl implements CustomerDetailService{
    public String saveCustomerDetail(Integer id, String customername, String address,String phonenumber) {
        try{
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO CUSTOMER_DETAILS (ID,CUSTOMER_NAME,ADDRESS,PHONE_NUMBER) VALUES " 
                    + "('"+id+ "',"+customername+ ",'"+address+"','"+phonenumber+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status!=1){
                return "Save failed";
            }else{
                return "Saved successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(CustomerDetailServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "Save failed";
        }
}
    public CustomerDetails readCustomerDetail(Integer Id){
        CustomerDetails customerDetails = null;
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "SELECT * FROM CUSTOMER_DETAILS WHERE ID="+Id;
            ResultSet resultSet = statement.executeQuery(query);
            while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                Integer customer_name = resultset.getInt("CUSTOMER_NAME");
                Integer address = resultSet.getInt("ADDRESS");
                Integer phone_number = resultset.getInt("PHONE_NUMBER");
                customerDetails = new CustomerDetails(id,customer_name,address,phone_number);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return customerDetails;
       
    }
     public List<CustomerDetails> getAllCustomerDetail(){
        List<CustomerDetails> customerDetails = new ArrayList<>();
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT CUSTOMER_DETAILS.ID,CUSTOMER_NAME,ADDRESS,PHONE_NUMBER,VERIFY FROM CUSTOMER_DETAILS JOIN CUSTOMER ON CUSTOMER.ID=CUSTOMER_DETAILS.ID";
        ResultSet resultSet = statement.executeQuery(query);
        
        while(resultSet.next()){
             Integer id = resultSet.getInt("ID");
                String mobNo = resultSet.getString("CUSTOMER_NAME");
                Integer complaintId = resultSet.getInt("ADDRESS");
                String simType = resultSet.getString("PHONE_NUMBER");
                CustomerDetails customerDetails = new CustomerDetails(id,customer_name,address,phone_number);
                customerDetailS.add(customerDetails);
        }
        
    }   catch (SQLException ex) {
            Logger.getLogger(CylinderDetailServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
    }
        return cylinderdetails;
    
    }
     
     public String updateCustomerDetail(Integer id,String customerName,Integer address,String phoneNumber) throws SQLException{
        try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "UPDATE CUSTOMER_DETAILS SET ID='"+id+"',CUSTOMER_NAME="+customerName+",ADDRESS='"+address+"',PHONE_NUMBER='"+phoneNumber+"';
        System.out.print(query);
        int update = statement.executeUpdate(query);
        if(update !=1)
            return "Update failed";
        else {
            Connection connection = getConnection();
            
            return "Updated successfully";
            }
    }
     public String deleteCustomerDetail(Integer id) {
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM CUSTOMER_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
           return "Delete failed";
        }
     
}

    private Connection getConnection() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}


