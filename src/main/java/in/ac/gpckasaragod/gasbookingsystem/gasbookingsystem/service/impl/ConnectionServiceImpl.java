/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.gasbookingsystem.gasbookingsystem.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author student
 */
public class ConnectionServiceImpl {
    public class ConnectionService {
    String jdbcUrl="jdbc:mysql://localhost:3306/";
    String databasename="GASBOOKINGSYSTEM";
    String connectionString=jdbcUrl+databasename;
    String username="root";
    String password="mysql";
    
    public Connection getConnection() throws SQLException{
    return DriverManager.getConnection(connectionString,username,password);
    }
}
}